// soal 1

let  luas = (p,l) => { return p*l;};
console.log(luas(10,5));

let keliling = (p,l) => { return ((p+l)+(p+l));};
console.log(keliling(10,5));

// soal 2

const newFunction = function literal(firstName, lastName){
    return {firstName,lastName,fullName(){
    alert(firstName + " " + lastName)
    return }}}
   newFunction("William", "Imoh").fullName()
  
// soal 3

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
   const {firstName, lastName, address, hobby} = newObject;
   console.log(firstName, lastName, address, hobby)

// soal 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)

// soal 5

const planet = "earth"
const view = "glass"
var after = `Lorem ${view} dolor sit amet,
 consectetur adipiscing elit, ${planet} `

console.log(after)



  